### General notations

- $\repar EF$ denotes the proposed parametrization of $E$ to $F$.  Since the
  parametrization is bijective, the following properties are satisfied:
  - $\forall y\in F,\;\exists x\in E, \; \repar EF(x)=y$.
  - $\forall \p{x_1,x_2}\in E^2, \;
              \brackets{\repar EF\p{x_1}=\repar EF\p{x_2}}\Rightarrow
                \brackets{x_1=x_2}$.

  We note $\repar FE=\repar EF^{-1}$.

- When the parametrization depends on a hyper-parameter (such as a scale
  parameter), we will note $\reparp sEF$ where $s$ is a hyper-parameter to choose.

- We note $\Logistic$ the probability logistic distribution on $\reels$. This
  probability distribution has the cumulative distribution function
  $x\mapsto\expit\p x=\frac1{1+\exp\p{-x}}$.

- We note $\Logistic_n$ the multivariate distribution on $\reels^n$ with
  independent components and with each marginal following a $\Logistic$ on $\reels$.


### Vectors notations

- Vector/matrix indices: in general, the convention used in Python will be used.
  The indices start at zero and end at $n-1$. Thus for $x\in\reels^n$ we have
  $x=\p{x_0,\ldots,x_{n-1}}$.

- $\concat$ denotes the concatenation of vectors. For $x\in\reels^n$,
  $y\in\reels^m$, we have $\concat(x,y)\in\reels^{n+m}$ with $\concat(x,y)$ =
  $\p{x_0,\ldots,x_{n-1},y_0,\ldots,y_{m-1}}$.

- Vector/matrix slices. For $x\in\reels^n$ we note:
   - $x_{i:j} = \p{x_k}_{k: i\le k<j} = \p{x_i,\ldots,x_{j-1}}$.
        Warning, as in Python, the upper index is not included.
   - $x_{:i} = x_{0:i} = \p{x_k}_{k: k<i} = \p{x_0,\ldots,x_{i-1}}$.
        Warning, as in Python, the upper index is not included.
   - $x_{i:} = x_{i:n} = \p{x_k}_{k: k\ge i} = \p{x_i,\ldots,x_{n-1}}$.
   - Same notation is used for matrices.

- Cumulated sum. For $x\in\reels^n$, we note $\cumsum\p x\in\reels^n$ the vector
  defined by:
  $$
    \cumsum\p x = \p{\sum_{i=0}^k x_i}_{k: 0\le k<n}
          = \p{x_0, x_0+x_1, x_0+x_1+x_2, \ldots, \sum_i x_i}
  $$

- Cumulated product. For $x\in\reels^n$, we note $\cumprod\p x\in\reels^n$ the
  vector defined by:
  $$
    \cumprod\p x = \p{\prod_{i=0}^k x_i}_{k: 0\le k<n}
          = \p{x_0, x_0x_1, x_0x_1x_2, \ldots, \prod_i x_i}
  $$

- Reversed vector. For $x\in\reels^n$, we note $\flip(x)\in\reels^n$ the vector
  defined by:
  $$
    \flip\p x
        = \p{x_{n-1},\ldots,x_0}
  $$

- We note respectively $\odot$, $\oslash$, and $\owedge$, the element-wise
  product, the element-wise division and the element-wise power. For
  $x,y\in\reels^n$:

   - $x\odot y = \p{x_iy_i}_{i: 0\le i<n}$
   - $x\oslash y = \p{\frac{x_i}{y_i}}_{i: 0\le i<n}$
   - $x\owedge y = \p{x_i^{y_i}}_{i: 0\le i<n}$

- For $n\in\natup$, we note $\range(n)$ the vector of $\reels^n$
  defined by:
  $$
  \range(n) = \p{k}_{k: 0\le k<n} = \p{0, 1, \ldots, n}
  $$


### Matrix notations

- Notation for diagonal and triangular matrices. All element missing
  will be zero. For example, we note:
  $$\begin{bmatrix}
            1&\\
            2&3\\
            4&5&6\\
            7&8&9&10\\
          \end{bmatrix}
          =
          \begin{bmatrix}
            1&0&0&0\\
            2&3&0&0\\
            4&5&6&0\\
            7&8&9&10\\
          \end{bmatrix}
  $$
  $$\begin{bmatrix}
            1&\\
            &2\\
            &&3\\
            &&&4\\
          \end{bmatrix}
          =
          \begin{bmatrix}
            1&0&0&0\\
            0&2&0&0\\
            0&0&3&0\\
            0&0&0&4\\
          \end{bmatrix}
  $$

- Notation for symmetric matrices. We note $(\text{sym})$ to indicate the
  symmetry of the matrix, all missing elements must be filled by symmetry. For
  example, we note:
  $$\begin{bmatrix}
            1&&&\!\!\!\!\!\!\!\!\text{(sym)}\\
            2&3\\
            4&5&6\\
            7&8&9&10\\
          \end{bmatrix}
          =
          \begin{bmatrix}
            1&2&4&7\\
            2&3&5&8\\
            4&5&6&9\\
            7&8&9&10\\
          \end{bmatrix}$$

- For $n\in\natup$, and $M\in\reels^{n\times n}$, we note
  $\operatorname{diag}(M)$ the diagonal vector of matrix $M$.
  $$
    \operatorname{diag}(M)=\p{M_{ii}}_{i:0\le i<n}
  $$

- For $n\in\natup$, and $x\in\reels^n$, we note
  $\operatorname{undiag}(x)$ the diagonal matrix $M$ which has $x$ as diagonal
  vector.
  $$\operatorname{undiag}(x)=
            \begin{bmatrix}
          x_0  \\
          & x_1 \\
          && \ddots \\
          &&& x_{n-1}
        \end{bmatrix}$$
