### Symmetric positive definite matrices {#sec-mat-spd}

The set of symmetric positive definite matrices of $\reels^{n\times n}$
is defined by:
$$\mathsf S^n_{++} = \braces{M\in\reels^{n\times n}: M=M^T \wedge \forall v\in\reels^n,\, v^TMv>0}$$

#### Construction of the parametrization

We will construct the parametrization starting from $\mathsf S^n_{++}$
to $\reels^k$ (with $k=\frac{n(n+1)}2$).

Considering a matrix $M\in\mathsf S^n_{++}$ which has a diagonal of
order of magnitude $s\in\reelsp^n$. By posing
$M'=D_s^{-\frac12}MD_s^{-\frac12}$, with $D_s=\operatorname{undiag}(s)$,
we obtain a positive definite symmetric matrix which has a diagonal of
order of magnitude $1$.

A symmetric positive definite matrix $M$ has a unique decomposition of
the form $M'=LL^T$ with

- $L$ a lower triangular matrix,

- $\operatorname{diag}(L)\in\reelsp^n$.

This decomposition is the Cholesky factorization. The constraint of
positivity of the diagonal is essential to obtain the uniqueness of the
Cholesky factorization. Thus, the Cholesky factorization defines a
bijection between $\mathsf S^n_{++}$ and the space of lower triangular
matrices with positive diagonal.

Consider the line $i$ of the matrix $L$, since $L$ is lower triangular,
only the first part of the line $i$ up to the diagonal is useful, it is
$l_i = L_{i,:i+1}\in\reels^{i+1}$. Using the matrix product, we find
that ${\|l_i\|}^2 = l_i^Tl_i = M'_{ii}$. So the order of magnitude of
${\|l_i\|}^2$ is $1$. From this, we deduce that the elements (in
absolute value) of $l_i$ have an order of magnitude of
$\frac1{\sqrt{i+1}}$.

So we introduce:

$$L' = \operatorname{undiag}\p{
    \p{\sqrt{i+1}}_{i:0\le i<n}
  }L$$

Introducing $l'_i$ in the same way as $l_i$ with
$l'_i = L'_{i,:i+1}\in\reels^{i+1}$, we have $l'_i = \sqrt{i+1}l_i$.
Thus the elements (in absolute value) of $l'_i$ have a magnitude of $1$.

So we obtain a matrix $L'$ with positive diagonal and all the elements
having in absolute value an order of magnitude $1$. It is therefore
sufficient to use $\logexpmu=\reparp{1}{\reelsp}{\reels}$ for the
diagonal, and the identity for the extra-diagonal terms.

We obtain the element of $\reels^{n(n+1)/2}$:

$$\concat\p{
    \logupexp\p{\operatorname{diag}\p{L'}},
    \concat\p{
      \p{L'_{i,:i}}_{i:0\le i<n}
    }
  }$$

The parametrization defined in this way is thus:

$$\begin{array}{ccrcl}
    \reparp s{\mathsf S^n_{++}}{\reels^{n(n+1)/2}}&\colon&
    \mathsf S^n_{++}&\longrightarrow&\reels^{n(n+1)/2} \\
    &&M&\longmapsto&g\p{\operatorname{undiag}\p{
      \p{\sqrt{i+1}}_{i:0\le i<n}
    }\operatorname{cholesky}\p{D_s^{-\frac12}MD_s^{-\frac12}}} \\
    \\
    \text{with g}&\colon&L&\longmapsto&
    \concat\p{
      \logexpmu\p{\operatorname{diag}\p L},
      \concat\p{
        \p{L_{i,:i}}_{i:0\le i<n}
      }
    } \\
    \\
    \text{with }&&D_s&=&\operatorname{undiag}(s)\\
  \end{array}$$ {#eq-spd}

We deduce the reciprocal:

$$\begin{array}{ccrcl}
    \reparp s{\reels^{n(n+1)/2}}{\mathsf S^n_{++}}
    &\colon&\reels^{n(n+1)/2}&\rightarrow&\mathsf S^n_{++} \\
    &&x&\longmapsto&
    D_s^{\frac12}L(x)L(x)^TD_s^{\frac12} \\
    \\
    \text{with }L&\colon&x&\longmapsto&
    \operatorname{undiag}\p{\p{\frac1{\sqrt{i+1}}}_{i:0\le i<n}}
    \p{
      \operatorname{diag}\p{\logupexp\p{x_{:n}}} + g\p{x_{n:}}}\\
    \\
    \text{with }g&\colon&y&\longmapsto&
      \begin{bmatrix}
        0 \\
        y_0 & 0 \\
        y_{1} & y_{2} & 0 \\
        y_{3} & \cdots & y_{5} & 0 \\
        \vdots & & & \ddots & \ddots \\
        y_{n(n-1)/2-n+1} & \cdots & \cdots & \cdots & y_{n(n-1)/2-1} & 0
      \end{bmatrix} \\
      \\
    \text{with }&&D_s&=&\operatorname{undiag}(s)\\
  \end{array}$$

#### Implementation details

When implementing, it is useful not to write products by diagonal
matrices as matrix products, but to write them as vector product
operations with broadcasting to reduce the number of operations.

#### Implementation example

```{python}
import numpy as np

def spd_matrix_to_reals(x, scale=1.0):
    assert len(x.shape) == 2 and x.shape[0] == x.shape[1]
    n = x.shape[-1]
    if hasattr(scale, "shape"):
        assert len(scale.shape) == 0 or (
            len(scale.shape) == 1 and scale.shape[0] == n
        ), "Non broacastable shapes"

    if hasattr(scale, "shape") and len(scale.shape) == 1:
        sqrt_scale = np.sqrt(scale)
        x_rescaled = x / sqrt_scale[:, None] / sqrt_scale[None, :]
    else:
        x_rescaled = x / scale

    y = np.linalg.cholesky(x_rescaled)
    y *= np.sqrt(np.arange(1, n + 1))[:, None]
    diag_values = y[(np.arange(n),) * 2]
    tril_values = y[np.tril_indices(n, -1)]
    return np.concatenate((logexpm1(diag_values), tril_values))

def reals_to_spd_matrix(x, scale=1.0):
    n = int((8 * x.size + 1) ** 0.5 / 2)
    assert (
        x.size == n * (n + 1) // 2
    ), f"Incorect size. It does not exist n such as n*(n+1)/2=={x.size}"
    if hasattr(scale, "shape"):
        assert len(scale.shape) == 0 or (
            len(scale.shape) == 1 and scale.shape[0] == n
        ), "Non broacastable shapes"
    y = np.zeros((n, n))
    y[(np.arange(n),) * 2] = log1pexp(x[:n])
    y[np.tril_indices(n, -1)] = x[n:]

    y /= np.sqrt(np.arange(1, n + 1))[:, None]
    z_rescaled = y @ y.T
    if hasattr(scale, "shape") and len(scale.shape) == 1:
        sqrt_scale = np.sqrt(scale)
        z = z_rescaled * sqrt_scale[:, None] * sqrt_scale[None, :]
    else:
        z = z_rescaled * scale
    return z
```

```{python}
#| echo: false
if check_implems:
    y = reals_to_spd_matrix(random_x_vector)
    rev_x = spd_matrix_to_reals(y)
    assert np.abs(rev_x-random_x_vector).max()<1e-10
    y2 = pcfn.reals_to_spd_matrix(random_x_vector)
    assert np.abs(y-y2).max()<1e-10
```
