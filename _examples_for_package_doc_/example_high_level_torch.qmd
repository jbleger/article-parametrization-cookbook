---
format:
    rst: default
execute:
  cache: true
jupyter: python3
bibliography: ../biblio.bib
crossref:
  ref-hyperlink: true
link-citations: true
---

{{< include _macros_.qmd >}}

### Inference using High-Level interface with PyTorch

#### Introduction
{{< include ../_examples_/_examples_hl_intro_.qmd >}}
{{< include ../_examples_/_examples_hl_torch_.qmd >}}
